\section*{Web Application Vulnerabilities}

Web applications are usually implemented in high-level languages.
This offers us protection against low-level vulnerabilities like buffer overflow.
However, there are lots of high-level attack vectors, and the attack surface is very large.
Input validation often causes issues.

Typically, the goals of web attacks are to tamper with data, elevate privileges, deny service or disclose sensitive information.

\subsubsection*{Basics}

URLS are formed of the scheme, host, path and then optionally the query and fragment sections.
URL or Percent-encoding is used in the query string.

Both GET and POST requests can send data to a server, GET via query parameters and POST in the request body.
POST requests are intended for operations that cause side effects.

HTTP is stateless, so sessions are required to recognise actions which should be attributed with a certain user.
Sessions are often represented using an ID, which is stored client-side in a cookie.

Sessions can be used for unauthenticated action (like shopping basket items) or to track user logins.

\subsubsection*{Attacks}

HTTPS is becoming the norm, however it is not immune to attacks.
Cryptographic issues (e.g.\ DROWN) and a lack of HSTS result in a very small risk of the connection being attacked.

There are still numerous ways to attack the server (application) instead.

\textbf{Open Redirects}: A query parameter is used to redirect the user to a certain page after an action (e.g.\ Login).
If this is not validated, a threat actor could provide a link which redirects to a malicious site once a login is performed.
This can be done by ensuring that only relative paths are used, or the path provided is on a whitelist.

\textbf{URL Jumping}: If the client-side is used to track which step of a process the user is on (e.g.\ purchasing an item), then it may be possible to skip steps.
This can be avoided by storing the current stage of the process server-side.

Typical authentication errors include:

\begin{itemize}
    \itemsep-0.75em
    \item Using BASIC or DIGEST auth methods without HTTPS
    \item Disclosing username validity
    \item Allowing weak passwords
    \item Allowing brute force attacks against accounts
    \item Not storing passwords securely
    \item Having default accounts/hard-coded credentials
\end{itemize}

To manage passwords securely, they should be hashed, NOT encrypted, using a secure hash function (e.g\ SHA-256 or larger); salted; made slow using iterations; and users must be promptly informed if a breach occurs.

Session IDs may be predictable by attackers, or stolen in a number of ways.
For example if a client doesn't support cookies, the session ID may be placed in the URL, but this makes it very open to stealing.

Sessions should be secured by always doing the following:

\begin{itemize}
    \itemsep-0.75em
    \item Generating IDs with a CSPRNG
    \item Never reusing session IDs
    \item Storing them in HTTP-only, Secure cookies
    \item Always using HTTPS
    \item Invalidate idle sessions and have a maximum lifetime
    \item Allow easy logout and limit concurrency
    \item Encrypt all client-side data
\end{itemize}

\textbf{Malicious XML Payloads:} Traditional web services use XML to pass messages.
An XML document sent to the server must be parsed to be processed.
This creates opportunities for exploitation, for example Exponential Entity Expansion.

\textbf{Cross Site Scripting (XSS)}: A type of injection attack where malicious scripts can be injected on a website that is otherwise trusted.

Reflected XSS occurs when data is received in an HTTP request and then immediately included in the response in an unsafe manner.
If an attacker can control a script ran in the user's browser, they can likely entirely compromise the user.
For example, the attacker could send a string containing all the user's cookies for the site to an application they control.

Stored XSS relies on the malicious JavaScript code being stored on the vulnerable server, often in a database.
It is rarer than reflected XSS, but can cause more damage as the payload may persist for a long time, and affect many people browsing the site.

XSS can be prevented by encoding HTML characters, or completely rejecting user provided HTML and only allowing things like Markdown.
Using \texttt{HttpOnly} cookies prevents them being accessed using JavaScript, so they can't be stolen via XSS\@.

\textbf{HTML Injection:} A frame can be injected to a website that an attacker controls.
That frame could display a message requesting a user re-login, to capture credentials.
A similar effect could be achieved by injecting a form for the user to fill out.
This attack can occur when user provided data is not properly handled, allowing arbitrary HTML to be inserted in a page.

\textbf{Cross Site Request Forgery (CSRF)}: An attack which abuses the fact that the browser will send an authentication server to the server when a request is made.
Because of this, if a user is logged into a site, and then they are sent to a malicious site which makes a POST request for the trusted site, it may succeed because the user has already authenticated.
CSRF can be prevented by generating a random nonce, and storing this with the user's session data on the server.
This nonce will be returned on pages in the browser using a hidden form field, and has to be present for a request to succeed.

Client-side validation is a usability/performance improvement, NOT a security aid - validation must also be performed on the server.
Validation may be performed as soon as possible to ensure that malicious data is stored in fewer places, but this requires trust that it is validated correctly.

When preventing XSS, values should be encoded on output.
This is because it is not only user input that needs encoding, but also database query results or file contents.
Also, if it is stored in the database encoded, non-web systems will have to handle this, and it can cause problems with fixed length database fields due to character expansion.

\subsubsection*{OWASP}

The Open Web Application Security Project - performs research and publishes information on web vulnerabilities and their mitigations.
