\section*{Firewalls}

A \textbf{firewall} is a security guard placed at the point of entry between a private network and the internet, which monitors all incoming and outgoing traffic.

Firewalls consist of a set of rules, and the decision as to whether a packet can continue depends on which rule it satisfies.

\textbf{Consistency issue}: Some rules may conflict; several rules may be satisfied with differing outputs. \\
\textbf{Completeness issue}: It is difficult to ensure that every package is considered, and handled. \\
\textbf{Compactness issue}: Rules may be redundant, or duplicated, creating inefficiencies.

\subsection*{Notation}

Packet: $n$-tuple, $\langle d_1 \ldots d_n \rangle$ of data. \\
Field, $F_i$: variable with a non-negative integer domain, denoted $D(F_i)$. \\
Each data item $d_i$ in a packet satisfies $d_i \in D(F_i)$. \\
$\Sigma$: set of all packets over fields $F_i \ldots F_n$. \\
Rule: $\langle Predicate \rangle \rightarrow \langle Decision \rangle$. \\
Predicate: boolean expression over $d_1 \ldots d_n$. \\
Decision $\in \{ accept, discard \}$ ($\{a, d\}$). \\
Firewall: sequence of rules $R_1 \ldots R_n$.

A packet \textit{matches} a rule $R_i$ iff the packet satisfies the predicate of $R_i$.
Two rules \textit{overlap} if there exists at least one packet which can \textit{match} both rules.
Two rules \textit{conflict} if they \textit{overlap} and have different decisions.
When two rules $R_{i}, R_{j}; i < j$ \textit{conflict}, the decision of $R_i$ is taken.
The last rule is called the `default rule' and is usually a tautology.

\subsection*{Firewall Decision Diagrams (FDD)}

An \textbf{FDD} is a DAG, which has a one root node with no incoming edges, and nodes with no outgoing edges, called terminal nodes.
Each node $v$ in the FDD is labeled with a field, $F(v)$, which is in $\{a,d\}$ for a terminal node, and in $F_i \ldots F_n$ otherwise.

An edge $e$ is labeled with a non-empty set of integers, $I(e)$, such that if $e$ is an outgoing edge of $v$, then $I(e) \subseteq D(F(v))$.
In simple terms, the set of integers that an edge is labeled with must be a subset of the domain of the field node $v$ relates to.

A directed path from the root to a terminal node is called a `decision path'.
No two nodes on a decision path can have the same label.

The set of all edges of node $v$, denoted $E(v)$, must follow these two conditions:

\textbf{Consistency}: $I(e) \cap I(e^{\prime}) = \emptyset$ \\
\textbf{Completeness}: $\cup_{e \in E(v)}I(e) = D(F(v))$

These two rules ensure all the edges from a node cover the whole domain of the field, and that two paths don't cover the same conditions.

\begin{figure}[ht!]
    \label{fig:fdd}
    \centering
    \begin{tikzpicture}[thick, level distance=1.5cm,
        level 1/.style={sibling distance=3cm},
        level 2/.style={sibling distance=1.5cm}]

        \node[circle, draw] (root) {$F_1$}
        child {node[circle, draw] (child1) {$F_2$}
        child {node[draw, minimum size=0.75cm] {$a$}}
        child {node[draw, minimum size=0.75cm] {$d$}}
        }
        child {node[circle, draw] (child2) {$F_2$}
        child {node[draw, minimum size=0.75cm] {$a$}}
        child {node[draw, minimum size=0.75cm] {$d$}}
        }
        child {node[circle, draw] (child3) {$F_2$}
        child {node[draw, minimum size=0.75cm] {$d$}}
        child {node[draw, minimum size=0.75cm] {$d$}}
        };

        % Arrows
        \draw[->] (root) -- node[above left=-2pt, font=\tiny, text width=1cm, align=center] {$[5,6]$} (child1);
        \draw[->] (root) -- node[left=-5pt, font=\tiny, text width=1cm, align=center] {$[7,8]$} (child2);
        \draw[->] (root) -- node[above right=-2pt, font=\tiny, text width=1cm, align=center] {$[1,4]$ $[9, 10]$} (child3);
        \draw[->] (child1) -- node[above left=-2pt, font=\tiny, text width=1cm, align=center] {$[3,4]$ $[6,8]$} (child1-1);
        \draw[->] (child1) -- node[above right=-2pt, font=\tiny, text width=1cm, align=center] {$[1,2]$ $[5,5]$ $[9,10]$} (child1-2);
        \draw[->] (child2) -- node[above left=-2pt, font=\tiny, text width=1cm, align=center] {$[3,4]$ $[6,8]$} (child2-1);
        \draw[->] (child2) -- node[above right=-2pt, font=\tiny, text width=1cm, align=center] {$[1,2]$ $[5,5]$ $[9,10]$} (child2-2);
        \draw[->] (child3) -- node[above left=-2pt, font=\tiny, text width=1cm, align=center] {$[1,5]$} (child3-1);
        \draw[->] (child3) -- node[above right=-2pt, font=\tiny, text width=1cm, align=center] {$[6,10]$} (child3-2);
    \end{tikzpicture}
    \caption*{An example FDD}
\end{figure}

An FDD maps each packet to a decision by running it through the FDD from the root until a terminal node is reached.
Each non-terminal node specifies a test on a packet field.
Each outgoing edge corresponds to some values for that field.

An edge is selected when the edge label contains the value of the packet field.
This process repeats until a terminal node is reached, the label of which determines the packet's fate.

% TODO: Traversal pseudocode?

Decision Path: represented by $\langle v_1 e_1 \ldots v_k e_k v_{k+1} \rangle$, where: \\
\hspace*{0.25cm} - $v_1$ is the root node, and \\
\hspace*{0.25cm} - $v_{k+1}$ is a terminal node, and \\
\hspace*{0.25cm} - each edge $e_i$ is a directed arc from $v_i$ from $v_{i+1}$.

A decision path represents the rule: \\
$F_i \in S_1 \land \ldots \land F_d \in S_d \rightarrow \langle decision \rangle$, where $decision$ is the label of the terminal node and
$S_i = I(e_j)$ if there exists a node $v_j$ in the decision path with field $F_i$, or $D(F_i)$ if this is not the case.

Each path (and its labels) represents a rule in the firewall.
There is only \textbf{one} rule that a packet $p$ will match in an FDD\@.

For FDD $f$, \\
$f.accept$ is the set of all packets accepted by $f$. \\
$f.discard$ is the set of all packets discarded by $f$. \\
$f.accept \cap f.discard = \emptyset$. \\
$f.accept \cup f.discard = \Sigma$.

Two FDDs $f$ and $f^{\prime}$ are equivalent iff the $accept$ and $discard$ properties are equal.
This equivalence relation, $f \equiv f^{\prime}$, is reflexive, symmetric and transitive.

\subsubsection*{FDD Reduction}

The greater the number of decision paths in the tree, the greater the number of rules in the firewall.
This increases the amount of time it takes for a decision to be made for any packet.
For this reason, we want to reduce the number of decision paths.

We can reduce an FDD using the concept of isomorphic nodes.
Two nodes $v$ and $v^{\prime}$ are isomorphic iff they are both terminal nodes with identical labels, OR,
they are both non-terminal nodes, which have a 1--1 mapping of their outgoing edges, such that every pair of corresponding edges has identical labels and point to the same node.

An FDD is \textbf{reduced} if 1) no node in the FDD has only one outgoing edge, 2) no two nodes are isomorphic, and 3) no two nodes have more than one edge between them.

% TODO: Reduction pseudocode?

\begin{figure}[ht!]
    \label{fig:fdd-reduced}
    \centering
    \begin{tikzpicture}[thick, node distance={20mm}]

        \node[circle, draw] (root) {$F_1$};
        \node[circle, draw] (child1) [below left of=root] {$F_2$};
        \node[draw, minimum size=0.75cm] (child1-1) [below left of=child1] {$a$};
        \node[draw, minimum size=0.75cm] (child1-2) [right of=child1-1, xshift=4cm] {$d$};

        % Arrows
        \draw[->] (root) -- node[above left=-2pt, font=\tiny, text width=1cm, align=center] {$[5,8]$} (child1);
        \draw[->] (root) -- node[above right=-2pt, font=\tiny, text width=1cm, align=center] {$[1,4]$ $[9, 10]$} (child1-2);
        \draw[->] (child1) -- node[above left=-2pt, font=\tiny, text width=1cm, align=center] {$[3,4]$ $[6,8]$} (child1-1);
        \draw[->] (child1) -- node[above=2pt, font=\tiny, text width=1cm, align=center] {$[1,2]$ $[5,5]$ $[9,10]$} (child1-2);
    \end{tikzpicture}
    \caption*{The example FDD, after reduction}
\end{figure}

\subsubsection*{FDD Marking}

\textbf{Note}: The reason for marking is because it allows us to consider one path as ``the rest''.
This means that instead of checking for presence in every interval, we can assume the field must have this value because it didn't match any other path.
We aim to mark the largest interval as ``the rest'' to minimise workload.

In a firewall, the order of the rules is also important.
We can do this using the concept of \textbf{FDD marking} -- we mark which transitions should be considered last.

$F_i \in S_1 \land \ldots \land F_d \in S_d \rightarrow \langle decision \rangle$ is `simple' iff every $S_i$, $1 \leq i \leq n$, is an interval of consecutive, non-negative integers.
Most firewalls require simple rules, so we want to minimise the number of simple rules generated from an FDD\@.
The number of simple rules from a marked FDD will always be less than or equal to the original FDD\@.

A \textbf{marked} version $f^{\prime}$ of FDD $f$ is the same as $f$, except one outgoing edge from each non-terminal node is marked ``all''.
$f$ and $f^{\prime}$ are equivalent because the labels of $f^{\prime}$ don't change.

The load of a non-empty set of integers $S$, denoted $load(S)$, is the minimum number of non-overlapping intervals that cover $S$.
For example, $load(\{1,2,3,5,8,9,10\}) = 3$, because we have an interval 1--3, 5--5, and 8--10.

In a marked FDD, the load of an edge $e$ is 1 if the edge is marked \textbf{all}, otherwise it is equal to $load(I(e))$.

The load of a marked FDD $f$, $load(f)$, is equal to the load of the nodes of $f$.

The load of a node $v$ in a marked FDD is defined as:

\vspace*{-0.75cm}

\begin{align*}
    load(v) =
    \begin{cases}
        1, \text{if } v \text{ is terminal}\\
        \sum_{i=1}^{k} load(e_i) \times load(v_i),\\ \text{if } v \text{ is non-terminal and } v \text{ has } k \text{ outgoing edges } \\ e_1 \ldots e_k \text{, which point to nodes } v_1 \ldots v_k
    \end{cases}
\end{align*}

% TODO: Marking pseudocode?

\begin{figure}[ht!]
    \label{fig:fdd-marked}
    \centering
    \begin{tikzpicture}[thick, node distance={20mm}]

        \node[circle, draw] (root) {$F_1$};
        \node[circle, draw] (child1) [below left of=root] {$F_2$};
        \node[draw, minimum size=0.75cm] (child1-1) [below left of=child1] {$a$};
        \node[draw, minimum size=0.75cm] (child1-2) [right of=child1-1, xshift=4cm] {$d$};

        % Arrows
        \draw[->] (root) -- node[above left=-2pt, font=\tiny, text width=1cm, align=center] {$[5,8]$} (child1);
        \draw[->] (root) -- node[above right=-2pt, font=\tiny, text width=1cm, align=center] {$[1,4]$ $[9, 10]$} (child1-2);
        \draw[->] (root) -- node[below right=-2pt, font=\small, text width=1cm, align=center] {\textbf{all}} (child1-2);
        \draw[->] (child1) -- node[above left=-2pt, font=\tiny, text width=1cm, align=center] {$[3,4]$ $[6,8]$} (child1-1);
        \draw[->] (child1) -- node[above=2pt, font=\tiny, text width=1cm, align=center] {$[1,2]$ $[5,5]$ $[9,10]$} (child1-2);
        \draw[->] (child1) -- node[below=2pt, font=\small, text width=1cm, align=center] {\textbf{all}} (child1-2);
    \end{tikzpicture}
    \caption*{The reduced FDD, after marking}
\end{figure}

The load of a marked FDD $f$, is equal to the load of the root of $f$.
A marked FDD with a smaller load will generate fewer simple rules.

\subsubsection*{Firewall Generation}

Starting with a marked FDD $f$, a depth-first traversal can be performed, such that for each non-terminal node $v$, the outgoing edge marked \textbf{all} of v is traversed after the other outgoing edges of $v$ have been traversed.
Whenever a terminal node is encountered, if $v_1 e_1 \ldots v_k e_k v_{k+1}$ is a decision path, then a rule should be outputted in the following manner:

$F_1 \in S_1 \land \ldots \land F_n \in S_n \rightarrow F(v_{k+1})$, where

\vspace*{-0.66cm}

\[
    S_i =
    \begin{cases}
        I(e_j), & \text{if the path has a node } v_j \text{ labelled with field } \\ & F_i \text{, and } e_j \text{ is not marked \textbf{all}} \\
        D(F_i), & \text{otherwise}
    \end{cases}
\]

\vspace*{-0.33cm}

And, $F_1 \in T_1 \land \ldots \land F_n \in T_n \rightarrow F(v_{k+1})$, where

\vspace*{-0.33cm}

\[
    T_i =
    \begin{cases}
        I(e_j), & \text{if the path has a node } v_j \\ & \text{labelled with field } F_i \\
        D(F_i), & \text{otherwise}
    \end{cases}
\]

The first predicate (referring to $S_i$) is called the \textbf{matching predicate}.
The second predicate (referring to $T_i$) is called the \textbf{resolving predicate}.

For the example FDD, the rules generated are as follows:

\vspace*{-0.5cm}

\begin{flalign*}
    \begin{array}{l@{~}l@{}l}
        r_1 &= F_1 \in [5,8] \land F_2 \in [3,4] \cup [6,8] & \rightarrow a \\
        r_1.mp &= F_1 \in [5,8] \land F_2 \in [3,4] \cup [6,8] \\
        r_1.rp &= F_1 \in [5,8] \land F_2 \in [3,4] \cup [6,8] \\\\
        r_2 &= F_1 \in [5,8] \land F_2 \in [1,10] & \rightarrow d \\
        r_2.mp &= F_1 \in [5,8] \land F_2 \in [1,10] \\
        r_2.rp &= F_1 \in [5,8] \land F_2 \in [1,2] \cup [5,5] \cup [9,10] \\\\
        r_3 &= F_1 \in [1,10] \land F_2 \in [1,10] & \rightarrow d \\
        r_3.mp &= F_1 \in [1,10] \land F_2 \in [1,10] \\
        r_3.rp &= F_1 \in [1,4] \cup [9,10] \land F_2 \in [1,10]
    \end{array}
\end{flalign*}

This generated firewall may still have redundant rules, which can be removed without changing the semantics of the firewall.
These should be removed, to improve efficiency.
The process of compacting a firewall is done by checking each rule in the firewall against the others, and checking that the resolving predicate of the first rule implies the matching predicate of the second.

In the example firewall, rule 2 is redundant, resulting in the following compacted firewall:

\vspace*{-0.5cm}

\begin{flalign*}
    \begin{array}{l@{~}ll}
        r_1 &= F_1 \in [5,8] \land F_2 \in [3,4] \cup [6,8] & \rightarrow a \\
        r_2 &= F_1 \in [1,10] \land F_2 \in [1,10] & \rightarrow d
    \end{array}
\end{flalign*}

Finally, firewall implementations typically require rules to be \textbf{simple}.
As explained above, this is when every $S_i$ is a consecutive non-negative interval.
This means that for rules with multiple intervals, these need to be split into separate rules.

Completing this process for the example firewall, we are left with a simple firewall, constructed from the initial FDD:

\vspace*{-0.5cm}

\begin{flalign*}
    \begin{array}{l@{~}l@{~}l}
        r_1 &= F_1 \in [5,8] \land F_2 \in [3,4] & \rightarrow a \\
        r_2 &= F_1 \in [5,8] \land F_2 \in [6,8] & \rightarrow a \\
        r_3 &= F_1 \in [1,10] \land F_2 \in [1,10] & \rightarrow d
    \end{array}
\end{flalign*}
