\section*{Cryptography}

We want to know if data has been altered, and if it is authentic.

\subsection*{Hash Functions}

Applying $H$ produces a fixed-length message digest or `hash' for any size of input, $x$.
For any $x$, $H(x)$ should be easy to compute.
A small change in $x$ should create a large change in $H(x)$ - avalanche effect.

\textbf{Preimage resistance:} Given a digest $h$, we can't find an $x$ such that $H(x) = h$ \\
\textbf{Second preimage resistance:} Given an input $x$, we can't find an input $y$, such that $y \neq x$ and $H(x) = H(y)$ \\
\textbf{Collision resistance:} We can't find a pair of inputs $\{x,y\}$ for which $H(x) = H(y)$

In an $n$-bit hash function, generating outputs from a space of $2^n$ possibilities, we can expect a ~50\% chance of finding a collision after having generated $2^n/2$ hashes.

\textbf{Note:} MD5 and SHA-1 are both no longer recommended due to demonstrated attacks.

\subsubsection*{Merkle-Damgård Construction}

Most hash functions (apart from SHA-3) use the MD construction.
The input is padded to be a multiple of the block size, and each block of the input is run through a compression function.
This compression function is a special block cipher.
Message blocks are used as the cipher key, and the cipher is used to encrypt the previous CF output value.
The result of the encryption is XORed with the previous value to produce the new output.
There is no previous value for the first block, so an \textbf{Initialization Vector} is used.

\subsubsection*{MACs}

If we want to prove data integrity we could include the hash of the data with the message.
The issue with this however, is that an attack could change the message AND hash, and the recipient wouldn't know.

MACs use a shared secret, known by the sender and receiver, to prevent this type of attack.
We need to choose a quality secret, share it securely, prevent replay attacks, compare MACs securely and prevent length extension attacks.

To compare MACs, a constant time compare should be used.
Otherwise, an attack could measure response time and construct a valid tag one byte at a time.

To avoid replay attacks, an incrementing message counter can be used.
If an attacker was to replay a message, they wouldn't have the correct message number, and so the message would be rejected.

Because the digest of the MD construction is equivalent to the full internal state at that stage of computation, computation can be continued with more (malicious) input.
The MAC for message $M_1$ is given by $H(S \mid\mid M_1 \mid\mid P_1)$.
If computation is continued with $M_2$ and $P_2$, the result is $H(S \mid\mid M_1 \mid\mid P_1 \mid\mid M_2 \mid\mid P_2)$.
This has the same value as a tag produced only for $M_1 \mid\mid P_1 \mid\mid M_2$.
Therefore, if we can craft a valid message in this format, the attack will succeed.

This can be prevented using HMACs.

\subsubsection*{HMACs}

HMACs use a nested hashing approach.
We derive an inner and outer secret by padding/hashing the secret $S$ to the size of a block, then XOR the result with some constants.
Finally, we calculate the HMAC by computing the following: $HMAC(S, M) = H(S_{OUT} \mid\mid H(S_{IN} \mid\mid M))$.

\subsubsection*{SHA-3}

New type of hash function which doesn't use MD principles.
The `Keccak' family of algorithms was selected, which use the Sponge construction.
It offers the same size outputs as SHA-2, so it can be dropped-in to replace.
Some part of the internal state never leaks into the final hash, which prevents \textbf{Length Extension}.

\subsection*{User Authentication}

In authentication, we use something you know (e.g.\ passwords, PINs), something you have (e.g.\ phone, security key) and something you are (e.g.\ biometrics).

Given the number of keys on a keyboard and typical password length, the possible password space is huge.
However, humans use a very limited set of the password space, write passwords down, and share them with others.
Passwords are also brute-forced, left as the default value, stolen over-the-shoulder etc.

Social engineering is done in mass in recent times through phishing.
Targeted attacks sometimes involve directly contacting an individual (e.g.\ over the phone).

When storing passwords, a hash should be stored instead of the password itself.
Hashes are fast by design, so they should be slowed down to limit brute-force.
This can be done using lots of iterations.
Rainbow tables of password-hash mappings can be used to crack passwords.
To prevent this a random salt value should be used.

Passwords are easy to implement, but relatively weak.
An easy way to add strength is with a second factor (2FA/MFA).
Examples are Google Authenticator, YubiKey, smart card.

Biometrics is most convenient for users, but can be expensive to implement.
They also cannot be revoked, so care should be taken as to how they are stored.
One way to address this is to combine the biometric data with a revocable data point, and store the hash of this.

\subsection*{Public Key Cryptography (PKC)}

Based on number theory, using a public and private key, which have a mathematical relationship.
One is for encryption, one for decryption - asymmetric.

It should be easy to encrypt with the public key, easy to decrypt with the private key, infeasible to determine the private key from the public key, and infeasible to recover the plaintext, given the publickey and ciphertext.

The most well known example of PKC is \textbf{RSA}.
It's widely used in industry, well established, and is based on the difficult in factoring the produce of two huge prime numbers.

More recently, Elliptic Curve Cryptography (ECC) has become more prevalent, and will likely take over from RSA\@.
It provides equal security to RSA with much smaller key sizes (e.g.\ 3072-bit RSA $\rightarrow$ 256-bit ECC).
It is faster at key and signature generation, but verification is slower.
In theory, it's more vulnerable to quantum attacks.

\subsubsection*{Attacking RSA}

This can be achieved through brute forcing all private keys, but this is easily stopped by using large enough numbers.
Finding prime factors is another method, which has been achieved up to RSA-250.

Another danger is shared prime numbers.
For example if one key is based on primes $p, q$ and another on $p, r$, it is trivial to recover all three primes given $pq$ and $pr$.

Finally, if the $e$ value is too small (many implementations use 65537), capturing a few random bits of the private key can result in full key discovery.

\subsubsection*{Digital Signatures}

PKC is often used in hybrid encryption schemes, e.g.\ TLS and digital signatures.

If a message is encrypted using the private key, instead of the public key, the result isn't secure because it can be decrypted by anyone who has the public key.
However, we can be sure that the owner of the private key did indeed produce this message (as long as the private key has been kept secure).

So if the hash of a message is encrypted using the private key, authenticity and integrity can be assured.

Ed25519 is an example algorithm, which uses the Edwards-curve DSA, with SHA-512 and Curve25519.
It creates small public keys and signatures, is resistant to hash collisions, is immune to CPU cache side-channel attacks and is supported as an auth open in OpenSSH.

\subsubsection*{Public Key Certificates}

If Alice gets an Email from Bob containing his public key, Alice could use this to share a symmetric key, or verify signatures from Bob.
However, Alice cannot be sure that this is indeed Bob's public key.

\includegraphics[width=\linewidth]{assets/pkc}

This can be solved using Public Key Certificates.
The public key and the owner ID can be transported in one of these certificates, which has been signed by a Certificate Authority (CA).
The CA is a trusted third-party.

TLS Certificates used in HTTPS are an example of this.
The server provides the browser with its public key in a certificate, signed by a CA\@.
The browser then uses the CA's public key to verify the certificate (and therefore public key).

The standard format for these certificates is \textbf{X.509}.