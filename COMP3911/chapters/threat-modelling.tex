\section*{Threat Modelling}

The goal of threat modelling is to understand a system's threat profile, facilitate secure design and implementation, guide code review/penetration tests and discover vulnerabilities.
When modelling threats, we can do it from the perspective of the assets, attackers or software itself.

\textbf{Assets}: ``The valuable things you have''.
These can be tangible, like user credentials, payment data, confidential data, etc...
or intangible things like system availability, company reputation.

Attacker lists can be used when brainstorming attacks.
However, they may not provide enough structure to help ascertain what an attacker might do.

When thinking about threats, it can be done with varying levels of structure.
These include simple brainstorming, exploring user scenarios for things that could go wrong, to things like data-flow diagrams and STRIDE classifications.

\subsection*{Data-flow Diagrams (DFDs)}

\includegraphics[width=\linewidth]{assets/dfd}

\textbf{Trust boundaries} occur when entities of different levels of privilege interact.
These can typically be placed around physical computers, VMs, network segments, etc.

\textbf{Entry and exit points} are places where control or data crosses a trust boundary.
Infrastructure entry points like config files are often overlooked when identifying these.

Threats tend to cluster around entry/exit points on trust boundaries.
Building these system diagrams allows us to systematically learn where to look for threats.

\subsection*{STRIDE}

\begin{itemize}
    \itemsep-0.75em
    \item Spoofing identity
    \item Tampering with data
    \item Repudiation
    \item Information disclosure
    \item Denial of service
    \item Elevation of privilege
\end{itemize}

STRIDE acts as a useful checklist when considering what might threaten parts of a DFD\@.
It makes it easier to understand the effect of threats, and assign them a priority.

Some elements of STRIDE are more prevalent at certain DFD elements:

\begin{table}[ht!]
    \centering
    \label{tab:stride}
    \begin{tabular}{|l|c|c|c|c|c|c|}
        \hline
        \textbf{DFD Element Type} & S & T & R & I & D & E \\
        \hline
        External Entity & \checkmark &  & \checkmark &  &  &  \\
        \hline
        Data Flow &  & \checkmark &  & \checkmark & \checkmark & \\
        \hline
        Data Store &  & \checkmark & ? & \checkmark & \checkmark & \\
        \hline
        Process & \checkmark & \checkmark & \checkmark & \checkmark & \checkmark & \checkmark \\
        \hline
    \end{tabular}
\end{table}

\subsection*{Attack Trees}

Start with a root node representing a threat.
Child nodes each represent a condition that must be true for the attack to succeed.
The path from a leaf node to the root is an attack vector.
A dashed line can indicate low likelihood.
Siblings implicitly have an OR relationship, AND is explicitly shown.

Attack trees can be represented as text, or in a machine-readable format like JSON, but these are harder for people to understand.

\subsection*{Risk Assessment}

One possible scale is \textbf{DREAD}.
For each threat we can estimate the following (on a 1--5 scale): Damage Potential, Reproducibility, Exploitability, Affected Users and Discoverability

\begin{figure}[ht!]
    \centering
    \includegraphics[width=\linewidth]{assets/attack-tree}
    \caption*{An attack tree}
\end{figure}

Some issues with DREAD are that it is highly subjective, the dimensions are all weighted equally, and arguably not all dimensions are useful -- why wouldn't you assume discoverability is always a 5?

The \textbf{Common Vulnerability Scoring Standard (CVSS)} is a scale widely used in industry.
It uses a 0.0--10.0 scale, using a base score which can be changed based on environmental factors.

\subsection*{Threat Mitigation}

Ideally every threat would be mitigated by design/implementation of the system.
But this isn't possible, so rank threats and address from the highest downwards.

\section*{Security Policies}

A \textbf{security policy} is a high level specification of the security properties a system should have.
They also allow us to clarify the security requirements for the system.

\textbf{Policy}: A concise, formalised set of goals, e.g.\ which user can access which object \\
\textbf{Mechanisms}: Basic components needed to satisfy a policy, e.g.\ cryptographic primitives \\
\textbf{Middleware}: How mechanisms are brought together to satisfy the policy, e.g.\ ACLs, cryptographic protocols

Policies can be expressed in high or low level languages.
At a high level, policy constraints are expressed abstractly, with the constraints independent of the enforcement mechanism.
At a low level, policy constraints are expressed in terms of program options, inputs, or specific characteristics of system entities.

A \textbf{security model} is a model that represents a particular policy or set of policies.

\subsection*{Access Control}

Objects are classified according to labels.
These levels are based on the impact if this data is compromised.
e.g.\ no impact = unclassified, high impact = top secret.

Users have clearances and are classified with the same labels.
These labels are given based on the level of trust for that user.

With these labels access control becomes simple.
A user can access an object only if they have a clearance equal to or higher than the object's label.
This can be formalised in the following manner:

$S$ is the set of subjects of the system, $O$ is the set of objects of the system.
We let $level(s) = l_s$ be the security level of subject $s$, and $level(o) = l_o$ be the security level of object $o$.

\textbf{Bell-LaPadula (BLP)} offers a way to enforce this access control in a computer system.
It is a Multi-level security (MLS) model.

It's \textbf{simple security policy} states no process can read data at a higher level (\textbf{no read up}). \\
A subject $s$ can read object $o$ if and only if $level(o) \leq level(s)$ and $s$ has permission to write to $o$.

The \textbf{star property} states no process can write data to a lower level (\textbf{no write down}). \\
A subject $s$ can write object $o$ if and only if $level(s) \leq level(o)$ and $s$ has permission to write to $o$.

The *-property is present to protect against possible trojans.
BLP is implemented through \textbf{mandatory access control (MAC)}.

Each subject and object is given a set of labels.
The labels are a set of markings, the highest of which is the $level$, mentioned above, and the remaining labels are the $categories$.

We can then define the binary relation, `dominates', which is denoted $D(a, b)$, such that:

$\forall a,b \in labels: D(a, b) \iff level(a) \geq level(b) \land categories(a) \supseteq categories(b)$

This relation is a partial order, because it is antisymmetric and transitive.

Given an appropriate set of labels, and two functions $join()$ and $meet()$, the labels can form a lattice structure.
The $join$ and $meet$ functions return the least upper bound, and greatest lower bound for a pair of labels (using the dominates relation) respectively.
Not all sets of labels will produce a lattice, however.

An example set of labels which will produce a lattice is the following:

(TS, \{A, B\}), (TS, \{A\}), (S, \{A, B\}), (S, \{A\}), \\(TS, \{\}), (S, \{\}), (U, \{\})

We define a predicate, $exec$, which takes a subject, object, and action.
$exec$ returns whether the subject can perform the action on the object.
The full definition of $exec$ forms a security policy.

$exec$ is then a formal specification for the behaviour of a \textbf{reference monitor}.

Formalising the properties, \textbf{no read up} can be shown as:

$\forall s \in \text{subjects}, o \in \text{objects}: \\ exec(s, o, read) \iff D(label(s), label(o))$

And \textbf{no write down} as:

$\forall s \in \text{subjects}, o \in \text{objects}: \\ exec(s, o, write) \iff D(label(o), label(s))$

\textbf{Basic Security Theorem}: Let $\Sigma$ be a system with a secure initial state $v_0$.
Let $T$ be a set of state transformations (exec).
If every element in $T$ preserves both the simple security property, and star property, then every $v_i, i \geq 0$ is secure.

\subsection*{Integrity}

BLP is designed to provide access control, \textbf{BIBA} is used for preserving integrity.
Confidentiality is about who can read data, integrity is about who can write/modify it.

The policies it has are the opposite to these in BLP -- BIBA is \textbf{no read down} and \textbf{no write up}.

Formalising the properties, \textbf{no read down} can be shown as:

$\forall s \in \text{subjects}, o \in \text{objects}: \\ exec(s, o, read) \iff D(label(o), label(s))$

And \textbf{no write up} as:

$\forall s \in \text{subjects}, o \in \text{objects}: \\ exec(s, o, write) \iff D(label(s), label(o))$

\subsection*{Chinese Wall Model}

This model is intended for a firm of professionals, where partners need to prevent conflicts of interest and insider trading.
One a partner provides services for a client of a given type, they can't consult for another of this type -- a Chinese Wall is put in place.

Unlike BLP, CW requires keeping track of state -- which object has been `contaminated' by which subject.
It can be specified with a simple security and star property similar to other models.

\textbf{Simple security}: Each subject can access objects from at most one company from any type. \\
\textbf{Star}: A subject can write to an object $o$, as long as all other objects the subject has dealt with are either the same company, or a `sanitised' object.

Sanitisation in this context is the removal of conflict, achieved through a trusted party.

\subsection*{Resurrecting Duckling Model}

Not all policies can be specified with permanent access control.
Some systems require a secure transient association.

For example, two devices A and B will have a transient connection.
We need to be able to authenticate, and then restart (resurrect) with a clean state (duckling).

This requires two states -- imprintable and imprinted.
In state 1, the system is ready to trust another, and in state 2 it is bound to trust a system.
The transition from 1 $\rightarrow$ 2 is imprinting, 2 $\rightarrow$ 1 is death -- either through reset, old age, or transaction completeness.

Security policies focus on who can access what, and when.
This is a form of access control.
We can implement this as a 2D matrix, with rows for subjects and columns for objects.
The value in the matrix represents the permission level.

This allows each subject to have a list of associates objects -- its capabilities.
Some policies may be expressed in terms of ACLs, but implemented using capabilities to be more efficient.
Capabilities can be implemented using proxies, revoking the rights of a subject just requires removal of the respective proxy.

\textbf{Role-based Access Control (RBAC)}: Instead of fine-grained access control, on a user-by-user basis, we can instead provide roles with certain permissions.

\section*{Execution Monitoring (EM)}

We can use Operating System based security to protect against threats.
Attacks are increasingly sophisticated, and OSes can be used to enforce security.
They can monitor executions and terminate processes if required (think \texttt{SIGSEGV}).

A secure system will satisfy some security policy.
This policy will define execution which is deemed unacceptable.

Not all policies are enforceable using EM\@.
The only information available is that which can be discovered by observing.
This limits us to past events; EM can't help predict what is going to happen.
EM isn't aware of alternative executions paths or trees, unlike a compiler.

\subsection*{Formalising EMs}

A system model will consist of the target system $T$ to be monitored, the security policy $P$, the EM enforcement mechanism $M$ (e.g.\ reference monitor, security kernel).
The goal is for $M$ to abort $T$ if it is going to violate $P$.

We can represent the executions of any target system as a set $E$ of all finite and infinite sequences.
$E$ is called the property or specification.

The executions of our target system $T$, $E(T)$ is a subset of $E$; $E(T) \subseteq E$.

Every property is the intersection of a safety property, and a liveness property.
For example, in mutual exclusion, the safety property is that only one process can access the object at a time, and the liveness property is that every requesting process will eventually get access.

A security policy $P$ is a predicate on sets of executions.
A target $T$ satisfies policy $P$ if and only if $P(E(T))$ is true.
In essence, $P$ rules out target executions which are unacceptable.

This means that $P(E(T)) = \forall e \in E(T) : P^{\prime}(e)$, where $P^{\prime}$ is a predicate on a single execution.
Since $P^{\prime}(e)$ is true, $e$ is in the policy $P$, which means that $P$ is a property.
This is because when we apply $P^{\prime}$ to $e$, we can decide whether it is a good execution or not.

Not every security policy is a property.
The membership of $e$ in $P$ may depend on another execution, $e^{\prime}$, which means we would need to compare with other executions (which is not possible here).

\textbf{Policies that are properties can be enforced through execution monitoring.}

What this means is that if an execution $E$ is unacceptable, then there exists an $E^{\prime}$ which is a prefix (denoted with $\sqsubseteq$) to $E$ which is also unacceptable.
In simple terms, once an execution is unacceptable, no execution is allowed to happen afterwards.

Violating executions can be characterised by bad prefixes, so they are considered \textbf{safety properties}.
Therefore, if a set of executions is not a safety property, then no EM mechanism is possible.

Safety properties can be specified using automata, where input symbols correspond to system events.
This however can be impractical, if the set of possible states or the transition function is very large.
For this reason, guarded command notation can be used instead.

Guarded commands take the format $\langle guard \rangle \rightarrow \langle command \rangle$, where the guard is a predicate, and the command is an action to be taken.
The guard refers to the current input symbol and variables that represent the automaton's state.
The command updates these variables.

To enforce these security policies, the target system $T$ can be executed with a simulation of this automaton.
When $T$ is initialised, we do the same for the automaton.
Each time $T$ makes an execution, we pass this to the automaton.
The automaton makes a transition on each step, until one which is not permitted, at which point $T$ is terminated.

Not all safety properties can be enforced either.
For example, the policy ``One process can't be denied access to a resource for more than $t$ time units''.
This is a safety property, but we cannot enforce it because we aren't able to stop time.
