\section*{Communication}

Below is a simple abstraction of communication:

\begin{figure}[ht!]
    \centering
    \begin{tikzpicture}[thick, node distance={50mm}]
        \node[circle, draw] (i) {node $i$};
        \node[circle, draw] (j) [right of=i] {node $j$};

        % Arrows
        \draw[->] (i) -- node[above, align=center] {Message $m$} (j);
    \end{tikzpicture}
    \label{fig:communication}
\end{figure}

In reality, it is much more complex.
There are various network operators and physical communication methods, all with different properties.

\textbf{Latency} is the time it takes for a message to arrive.
Intra-datacenter this may be 1ms, between continents it could be hundreds of milliseconds.

\textbf{Bandwidth} is data volume per unit time.
For home broadband the upper limit is usually 300 Mbps, 5G cellular can reach 20 Gbps.

The amount of time $T$ it would take to send a message of size $S$ across a network with bandwidth $B$ and latency $L$ is equal to:

\vspace*{-0.5cm}

\[
    T = \frac{S}{B} + L
\]

Middleware should offer a rich set of communication protocols, data marshalling functions, naming and security protocols, as well as scaling mechanisms, like replication and caching.

\textbf{Transient communication} is where the communication server will discard a message when it can't be delivered to the next server.
\textbf{Persistent communication} is where the communication server will store the message for as long as is required to deliver it.

Client-server computing is usually based on transient synchronous communication.
Both parties have to be active at the time of communication, and the client will block until it receives a reply.
The server simply waits for incoming requests and processes them.

Message-oriented middleware aims to provide persistent asynchronous communication.
Processes send messages, which are queued.
The sender doesn't need to wait for an immediate reply.
This communication style can improve fault-tolerance.

\subsection*{Remote Procedure Call (RPC)}

Developers are used to the concept of calling procedures, and well engineered ones execute in isolation.
For this reason, it is viable for procedures to occur on a remote machine.

\includegraphics[width=\linewidth]{assets/rpc}

Packing parameters into a message is called \textbf{marshalling}.
Unpacking these parameters is \textbf{unmarshalling}.
The process of taking a message and converting it into bytes to be sent on the wire is called \textbf{serialisation}.
(Un)marshalling is the whole process of converting to and from machine-specific format.
This process needs to account for how different machines represent data types, which encodings they use, and even byte order.

\textbf{Note:} Big-endian is \textbf{MSB first}.
Little-endian is \textbf{LSB first}.

\textbf{Asynchronous RPC} builds on traditional RPC, but the client need not wait for a response from the server.
It can continue performing other tasks, and will receive the reply via callback when it is ready.

\subsection*{Message Passing Interface (MPI)}

Message-oriented primitives were required to build efficient applications, but traditionally high performance multicomputers would come with proprietary communication libraries.
\textbf{MPI} is designed for parallel applications, assumes communication is happening between a group of known processes, and makes use of the underlying network.

MPI provides intuitive primitives to enable message passing between processes.

\subsection*{Message-Queueing Model}

This is the concept of asynchronous persistent communication using middleware-level queues.
The following primitives are used:

\begin{tabular}{|p{0.2\linewidth}|p{0.7\linewidth}|}
    \hline
    \textbf{Primitive} & \textbf{Description} \\
    \hline
    Put & Append a message to a specified queue \\
    \hline
    Get & Block until the specified queue is non-empty, and remove the first message \\
    \hline
    Poll & Check a specified queue for messages, and remove the first -- Never block \\
    \hline
    Notify & Install a handler to be called when a message is put into the specified queue \\
    \hline
\end{tabular}

This allows for loosely-coupled communication.
It doesn't matter whether the client and server are both running, neither are, or one or the other is, messages will always be published and consumed whenever possible.

\subsection*{Advanced Message Queueing Protocol}

\textbf{AMQP} provides a platform-agnostic method for safely transporting information between applications, regardless of setting.
Many operating systems provide AMQP implementations, and cloud-hosted offerings are available.

Message-broker software offerings like \textbf{RabbitMQ} and \textbf{ActiveMQ} support AMQP\@.

\subsection*{Openstack}

\textbf{Openstack} is a Cloud Virtual Infrastructure Manager.
It uses RabbitMQ for message passing and MySQL for storage.
API requests are validated and routed through a queue, with workers listening to queues based on role.
