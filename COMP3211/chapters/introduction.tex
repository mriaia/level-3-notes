A \textbf{distributed system} is a collection of autonomous computing elements, that appear to its users as a single coherent system.

Alternatively, a system in which components located at networked computers communicate and coordinate actions only by \textbf{passing messages}.

For communication to occur by message passing, networking is required.
If nodes are autonomous, there's no concept of a global clock -- synchronisation is required.
Users don't know where exactly their data is being stored, replicated, or served from.

\textbf{Middleware} is key to distributed systems.
It contains commonly used components and functions, so applications don't have to implement them over and over.
This middleware layer extends over multiple machines.

\textbf{Transparency} is the concept of things being invisible to developers, users, etc.
Traditionally a lot of code is required for networking (e.g.\ dealing with TCP/IP).
Instead, it is preferable for the system to operate as if the client and server are parts of one centralised system.

Middleware is positioned in a way such that it hides the heterogeneity of the underlying platforms from applications.

\begin{table}[ht!]
    \centering
    \label{tab:transparency}
    \begin{tabular}{|p{0.2\linewidth}|p{0.7\linewidth}|}
        \hline
        \textbf{Type} & \textbf{Description} \\
        \hline
        Access & Hide differences in data representation and how an object is accessed \\
        \hline
        Location & Hide where an object is located \\
        \hline
        Migration & Hide that an object may move to another location \\
        \hline
        Relocation & Hide that an object may be moved to another location while in use \\
        \hline
        Replication & Hide that an object is replicated \\
        \hline
        Concurrency & Hide that an object may be shared by several competitive users \\
        \hline
        Failure & Hide the failure and recovery of an object \\
        \hline
    \end{tabular}
    \caption*{Types of transparency}
\end{table}

Systems should conform to well-defined interfaces, easily interoperate, support portability of applications, and be easily extensible.

A DS is scalable in \textbf{size} if it remains effective after a significant increase in users/resources.
When scaling \textbf{geographically}, synchronisation and communication can become an issue.
\textbf{Administratively}, we need to be careful of different policies across administrative domains.

To scale effectively, we need to be able to hide communication latencies from users, and distributing a task into domains managed by various administrations can help to provide a better running service.
Replication across a DS helps increase availability and balance component load, however keeping multiple copies can lead to inconsistencies and requires global synchronisation.

\textbf{Centralised systems} are those which are made of one component, with non-autonomous parts.
They have a single point of control, and therefore failure.
\textbf{Decentralised systems} have multiple autonomous components, with software running in concurrent processes.

Many false assumptions are made when dealing with distributed systems:

\begin{enumerate}
    \itemsep-0.75em
    \item The network is reliable
    \item The network is secure
    \item The network is homogenous
    \item Topology doesn't change
    \item Latency is zero
    \item Bandwidth is infinite
    \item Transport cost is zero
    \item There is one administrator
    \item All clocks are synchronised
\end{enumerate}

\section*{Types of Distributed System}

\subsection*{High Performance Distributed Computing Systems}

\subsubsection*{Cluster Computing}

This is essentially a group of high-end systems connected via Local Area Network.
The systems are homogenous (same OS and same/very similar hardware).
A computing cluster will have a single master node, and many compute nodes.

\subsubsection*{Grid Computing}

This is the concept of having lots of nodes from all over.
It allows for flexible, secure, coordinated resource sharing among dynamic collections of individuals and organisations.
It enables communities called ``Virtual Organisations'' to share geographically distributed resources to pursue common goals.

\subsubsection*{Cloud Computing}

An information technology infrastructure in which computing resources are virtualised, and offered as a service.
There is 4 distinct layers -- Hardware, Infrastructure, Platform, and Application.

\subsection*{Distributed Information Systems}

Organisations often have many networked applications, but interoperability is difficult.
A basic way to tackle this problem is to have clients combine requests for different applications, collect the results and then build a coherent response for the user.
This led to \textbf{Enterprise Application Integration (EAI)}.

An example is the distributed processing of transactions.
The \textbf{ACID} properties apply: Atomic, Consistent, Isolated, Durable.

To ensure transactions are processed correctly, a Transaction Processing Monitor (TPM) can be used.
This is responsible for coordinating transactions which are distributed across multiple servers.
\textbf{If one part of a transaction fails, the whole transaction is rolled back.}

Middleware offers communication facilities to enable application integration.
These include \textbf{Remote Procedure Call (RPC)} and \textbf{Message Oreiented Middleware (MOM)}.

\subsection*{Distributed Pervasive Systems}

These are the next-generation of distributed systems, with a focus on nodes which are often, small, portable, and sometimes embedded in larger systems, characterised by the fact that the system naturally blends into the user's environment.
This is often referred to as the \textbf{Internet of Things (IOT)}.

\subsubsection*{Ubiquitous Computing Systems}

In these systems, devices are networked, distributed, and accessible in a transparent manner.
The interactions with users is highly unobtrusive, the system is aware of user context to optimise interaction, they are autonomous and self-managed, requiring no human interaction, and they can handle a wide range of dynamic actions.

\subsubsection*{Mobile Computing Systems}

These feature a wide variety of mobile devices, including phones, tablets, GPS trackers, etc.
The devices in these systems are expected to change location over time.
Communication may become difficult, with no stable routes, or sometimes no guaranteed connection at all.
This results in a requirement for disruption-tolerant networking.

\subsubsection*{Sensor Networks}

These consist of nodes with sensors attached, and a network may be made of hundreds or thousands of nodes.
The nodes are typically simple, with small memory, compute and communication capacity.
They are often battery powered, and some even battery-less.
When communicating with sensors, there is a decision to be made whether to have all sensors send their readings to a central processing node, or for them to process and store their own data, which they then return in response to queries.
